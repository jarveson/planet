#pragma once

#include <string>
#include <vector>
#include "MaterialData.h"

struct Material {
    std::vector<MaterialData> matData;
};